# Pig-Cell-Pixel-Shooter

Small Godot-CS project, focused on extensibility and learning **C#** as we go.

## Game Description
*Mobile* (and maybe later on `PC`) game. Tap the enemies, throw weapons, cast spells and shield yourself from the hordes of enemies.

Level up your character and improve your arsenal, until you are free to escape from the hole you were forced to live in!

# Author(s)
*  Carlos A. Parra F. (`carlos.a.parra.f@gmail.com`)

# Contributing

Write me an email or just even send some merge requests.I will open a discussion, if it gets so far.

# Dependencies
If you want to code yourself and import the project on your own machine, use [Godot with C# support](https://godotengine.org/download/linux). Note that this will compile for Android only if you use verion **3.2.1****** or later. Install other C# specific dependiencies based on the instructions in Godot's website.

# Guidelines
Be nice and enjoy!