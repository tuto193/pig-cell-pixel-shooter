class_name BaseRoom
extends Node2D

export var size := Vector2(32, 18)
export(float, 0 , 1) var ground_probability := 0.1


# Private variables
onready var _tile_map_walls : TileMap = $Walls
onready var _tile_map_floor : TileMap = $Floor
var _rng := RandomNumberGenerator.new()

# Path walked by enemies
enum Access {
	NOT_VISITED,
	VISITED,
	DEAD_END,
}

enum Direction {
	LEFT,
	RIGHT,
	FORWARD
}

func setup() -> void:
	# Sets the game window size to twice the resolution of the world.
	var map_size_px := size * _tile_map_floor.cell_size
	get_tree().set_screen_stretch(
		SceneTree.STRETCH_MODE_2D, SceneTree.STRETCH_ASPECT_KEEP, map_size_px
	)
	OS.set_window_size(2 * map_size_px)


func generate() -> void:
	# Although there's no other nodes to use these signals, we're including them
	# to show when and how to emit them.
	# Watch our signals tutorial for more information.
	generate_floor()
	generate_walls()


func generate_walls() -> void:
	# Fills the outer edges of the map with the border tiles.
	# Randomly selects from the tiles marked as `Cell.OUTER` using the function `_pick_random_texture`.
	# The two nested loops below fill the outer columns and outer row
	# of the map, respectively.

	# First the top corners
	var index:int = 2 # top left corner
	for x in [0, size.x - 1]:
		_tile_map_walls.set_cell(x, 0, index + 1) # the small ceiling thingy
		_tile_map_walls.set_cell(x, 1, index) # the small ceiling thingy
		index = 4 # then the top right corner

	# Then the bottom ones
	index = 0
	for x in [0, size.x - 1]:
		_tile_map_walls.set_cell(x, int(size.y) - 1, index) # the small ceiling thingy
		index += 1

	# side walls
	index = 6 # left side
	for x in [0, size.x - 1]:
		for y in range(2, size.y - 1):
			_tile_map_walls.set_cell(x, y, index)
		index += 1 # right side

	# horizontal walls
	var normal_wall_prob = 0.5
	for x in range(1, size.x - 1):
		_tile_map_walls.set_cell(x, int(size.y) - 1, 33)
		_tile_map_walls.set_cell(x, 0, 33)
		var cell = 8 if _rng.randf() < normal_wall_prob else _pick_random_wall_tile()
		_tile_map_walls.set_cell(x, 1, cell)


func generate_floor() -> void:
	# Fills the inside of the map the inner tiles from the remaining types: `Cell.GROUND` and `Cell.OBSTACLE` using the
	# `get_random_tile` function that takes the probability for `Cell.GROUND` tiles to have some more control
	# over what types of tiles we'll be placing.
	var normal_floor_prob:float = 0.7
	for x in range(0, size.x):
		for y in range(1, size.y):
			var cell := 0 if _rng.randf() < normal_floor_prob else _pick_random_floor_tile()
			_tile_map_floor.set_cell(x, y, cell)


func _pick_random_floor_tile() -> int:
	# Returns the id of the cell in the TileSet resource.
	return _rng.randi_range(0, 7)


func _pick_random_wall_tile() -> int:
	# Returns the id of the cell in the TileSet resource.
	return _rng.randi_range(8, 10)


func _ready() -> void:
	_rng.randomize()
	setup()
	generate()
