class_name PathGenerator
extends Node2D

var size:Vector2

func generate_path(_size:Vector2, _random_start := true, _random_end := true) -> Array:
	var _current_depth:int = 0
	var _path:Array = _init_empty_path(_size)
	return []



func _init_empty_path(_dimensions:Vector2) -> Array:
	var path:Array = []
	for _y in range(int(_dimensions.y)):
		var row:Array = []
		# for _x in range(int(_dimensions.x)):
		# 	row.append(Access.NOT_VISITED)
		path.append(row)
	return path
