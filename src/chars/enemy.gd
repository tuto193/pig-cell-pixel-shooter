class_name Enemy
extends KinematicBody2D

# References to classes for easier typing
const PlayerAttack = preload("player_attack.gd")
const Player = preload("player.gd")


# The player node
onready var _player:Player = get_parent().get_node("Player")

onready var attack_range:Area2D = $Range

onready var animated_sprite:AnimatedSprite = $AnimatedSprite

onready var hitbox:CollisionShape2D = $Hitbox

enum State {
	MOVING,
	WAITING,
	DEAD,
}

var _current_state

enum StatusEffect {
	POISONED,
	FROZEN,
	BURNING,
	CONFUSED,
	INFECTED,
	ELECTRIFIED,
	BLEEDING,
}

var status_effects:Array

export var speed:float

export var health:float

onready var stun_timer:Timer = $StunTimer

onready var attack_timer:Timer = $AttackTimer

export var stun_time:float = 0.5
export var attack_interval:float = 1.0
export var attack_damage:float = 1.0

func _deal_damage_to(_body:PhysicsBody2D) -> void:
	pass

func _get_hit_by_attack(_attack:PlayerAttack) -> void:
	pass

func fucking_die() -> void:
	pass
