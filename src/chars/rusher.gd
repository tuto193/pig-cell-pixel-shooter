class_name Rusher
extends "enemy.gd"

var last_state = Enemy.State.MOVING

func _ready() -> void:
	self.health = 30.0
	self.speed = 50.0
	self._current_state = Enemy.State.MOVING
	var _error = self.attack_range.connect("body_entered", self, "_on_AttackRange_body_entered")
	_error = self.attack_range.connect("body_exited", self, "_on_AttackRange_body_exited")
	stun_timer.one_shot = true
	_error = stun_timer.connect("timeout", self, "_on_StunTimer_timeout")
	attack_timer.one_shot = true
	self.animated_sprite.play("run")


func set_state_moving() -> void:
	if self._current_state != Enemy.State.DEAD:
		self._current_state = Enemy.State.MOVING


func set_state_waiting() -> void:
	if self._current_state != Enemy.State.DEAD:
		self._current_state = Enemy.State.WAITING


func _physics_process(_delta:float) -> void:
	var _velocity = _player.global_position - self.global_position
	# print(
	# 	"Player pos \t{0}\nRusher pos \t {1}\nVelocity \t{2}\nspeed \t{3}".format([
	# 		_player.global_position,
	# 		self.global_position,
	# 		_velocity,
	# 		speed,
	# 	])
	# )

	match self._current_state:
		Enemy.State.MOVING:
			_velocity = _velocity.normalized() * speed
		Enemy.State.WAITING:
			_velocity = Vector2(0, 0)
		Enemy.State.DEAD:
			# Play death-animation and free resources
			_velocity = Vector2(0, 0)

	var _vel = move_and_slide(_velocity)

func _get_hit_by_attack(_attack:PlayerAttack) -> void:
	# TODO
	last_state = _current_state
	self._current_state = Enemy.State.WAITING
	stun_timer.start(stun_time)
	animated_sprite.play("stunned")
	var last_health = health
	self.health -= _attack.damage
	print("Got hit by {0}. \nHealth went from {1} to {2}".format([
		_attack.damage,
		last_health,
		health,
	]))
	# play "hurt" animation
	if self.health <= 0:
		self._current_state = Enemy.State.DEAD
		fucking_die()

func _deal_damage_to(body:PhysicsBody2D) -> void:
	if body != null and body.has_method("take_damage"):	# damageable body
		# TODO: Play a sound when hitting that object
		body.call("take_damage", attack_damage)


func _on_AttackRange_body_entered(_body:PhysicsBody2D) -> void:
	# TODO
	if _body is Player:
		# If the player goes out of reach again, disconnect this
		# Only attack players
		set_state_waiting()
		if not attack_timer.is_connected("timeout", self, "_on_AttackTimer_timeout"):
			var _error = attack_timer.connect("timeout", self, "_on_AttackTimer_timeout", [_body])
		attack_timer.start(attack_interval)
		animated_sprite.play("attack")


func _on_AttackRange_body_exited(body:PhysicsBody2D) -> void:
	if body is Player:
		set_state_moving()
		if attack_timer.is_connected("timeout", self, "_on_AttackTimer_timeout"):
			attack_timer.disconnect("timeout", self, "_on_AttackTimer_timeout")
		attack_timer.stop()
		animated_sprite.play("run")


func _on_AttackTimer_timeout(body:PhysicsBody2D) -> void:
	_deal_damage_to(body)


func on_StunTimer_timeout() -> void:
	set_state_moving()
	self.animated_sprite.play("run")


func _on_StunTimer_timeout() -> void:
	_current_state = last_state
	match _current_state:
		Enemy.State.MOVING:
			animated_sprite.play("run")
		Enemy.State.WAITING:
			animated_sprite.play("attack")


func fucking_die() -> void:
	animated_sprite.play("dead")
	if not animated_sprite.is_connected("animation_finished", self, "queue_free"):
		var _error = animated_sprite.connect("animation_finished", self, "queue_free")
