class_name TapShot
extends "player_attack.gd"

var hitbox:CollisionShape2D
var timer:Timer
var trajectory:Vector2
export var speed:float = 100
onready var animation_player:AnimationPlayer = $AnimationPlayer

func on_TapShot_entered(body:PhysicsBody2D) -> void:
	print("Shot somebody")
	animation_player.stop()
	animation_player.play("fade")
	speed = 0
	if body != null and body.has_method("_get_hit_by_attack"):
		body.call("_get_hit_by_attack", self)
		hitbox.call_deferred("set", "disabled", true)


func on_timer_timeout() -> void:
	self.queue_free()


func init(target:Vector2) ->void:
	self.trajectory = target

func _physics_process(_delta:float) -> void:
	self.position = self.position + (trajectory * speed * _delta)


func _ready() -> void:
	hitbox = get_node("CollisionShape2D")
	timer = get_node("Timer")
	var _error = timer.connect("timeout", self, "on_timer_timeout")
	_error = self.connect("body_entered", self, "on_TapShot_entered")
	animation_player.play("default")
