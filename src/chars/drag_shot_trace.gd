class_name DragShotTrace
extends "player_attack.gd"

const LONGEST:float = 250.0
const SHORTEST:float = 30.0

var sprite:Sprite

var collision_shape:CollisionShape2D

var trace_shape:CapsuleShape2D

var last_direction:Vector2

func init(start_pos:Vector2) -> void:
	self.position = start_pos
	self.last_direction = Vector2(self.position.y, -self.position.x)


func _ready() -> void:
	sprite = get_node("Sprite")
	collision_shape = get_node("CollisionShape2D")
	trace_shape = load("res://chars/DragShotTrace.tscn::1")


func update_scale(new_dir:Vector2) -> void:
	var actual_scale:Vector2 = new_dir - self.position
	if actual_scale.length() == 0:
		actual_scale = last_direction

	actual_scale = actual_scale.normalized() * LONGEST
	var re_scale:float = actual_scale.length()

	self.sprite.scale = Vector2(re_scale, re_scale)/40;
	var actual_rot:float = (actual_scale.angle() * 180 / PI) + 90
	self.sprite.rotation_degrees = actual_rot

	self.collision_shape.position = self.position + (actual_scale / 2)
	self.collision_shape.rotation_degrees = actual_rot
	self.trace_shape.height = re_scale / 2

	last_direction = actual_scale
