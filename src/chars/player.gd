#Player class for handling the attacking and getting hit
class_name Player
extends StaticBody2D


const DragShotTrace = preload("drag_shot_trace.gd")
const InputEventSingleScreenTap = preload("res://CustomInputEvents/InputEventSingleScreenTap.gd")
const InputEventSingleScreenTouch = preload("res://CustomInputEvents/InputEventSingleScreenTouch.gd")
const InputEventSingleScreenDrag = preload("res://CustomInputEvents/InputEventSingleScreenDrag.gd")
const InputEventScreenPinch = preload("res://CustomInputEvents/InputEventScreenPinch.gd")
const InputEventScreenTwist = preload("res://CustomInputEvents/InputEventScreenTwist.gd")
const InputEventMultiScreenDrag = preload("res://CustomInputEvents/InputEventMultiScreenDrag.gd")
# signals, to communicate with external nodes

var _attacking_mutex:Mutex
## cooldown timers for each of the different attacks

# Timers for the attacks
var _tap_attack_cooldown_timer:Timer
var _drag_attack_cooldown_timer:Timer
var _touch_attack_cooldown_timer:Timer
var _zoom_attack_cooldown_timer:Timer
# And their respective cooling times
var _tap_attack_cooldown:float = 0.69
var _drag_attack_cooldown:float = 0.69
var _touch_attack_cooldown:float = 0.69
var _zoom_attack_cooldown:float = 0.69

# Tapshot scene for instancing the attack
var _Tap_Shot:= preload("res://chars/TapShot.tscn")

# Initial position of a drag. Needed to create a vector with it and the final position
var _init_drag_pos:Vector2
var _initial_drag_set:bool = false
var _drag_shot_trace_scn:= load("res://chars/DragShotTrace.tscn")
var _drag_shot_trace:DragShotTrace
var _release_drag_timer:Timer
var _trace_wait_time:float = 0.3

# Touch attack properties, for during and before it is instanced


func _init()->void:
	_attacking_mutex = Mutex.new()
	_drag_shot_trace = null
	_release_drag_timer = Timer.new()
	_release_drag_timer.one_shot = true
	_release_drag_timer.wait_time = _trace_wait_time
	var _error = _release_drag_timer.connect("timeout", self, "_on_ReleaseDragTimer_timeout")
	self.add_child(_release_drag_timer)


# Called when the node enters the scene tree for the first time.
func _ready()->void:
	var _error
	#  = InputManager.connect("single_tap", self, "tap_attack", [])
	# _error = InputManager.connect("single_drag", self, "trace_drag", [])
	# _error = InputManager.connect("single_touch", self, "touch_attack", [])
	# _error = InputManager.connect("pinch", self, "zoom_attack", [])


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass


# Translate the coordinates of a touch input from the screen, to the actual coordinates
 #on the camera.
 # Returns:
 #	(Vector2): the scene coordinates of the input vector
func _touch_to_camera_coords(scr_pos:Vector2)->Vector2:
	var camera:Camera2D = get_parent().get_node('Camera2D')
	var scaling:Vector2 = camera.get_zoom()
	var scaled_scr_pos:Vector2 = Vector2(scr_pos.x * scaling.x, scr_pos.y * scaling.y)
	return camera.get_camera_position() + scaled_scr_pos


# Create an instance of Tap_Shot attack at the touched pos.
 #   Returns: void
func tap_attack(_event) ->void:
	if _attacking_mutex.try_lock() != OK:
		return
	var tap_pos:Vector2 = _touch_to_camera_coords(_event.position)
	var trajectory = (tap_pos - self.position).normalized()
	var tap_shot:= _Tap_Shot.instance()
	# shoot in the right direction
	tap_shot.init(trajectory)
	tap_shot.set_global_position(self.position + (trajectory * 20))
	get_parent().add_child(tap_shot)
	#string formatting
	# print("Instance of TS is at {0}\n\t It was tapped at {1}\n\t at global {2}".format([tap_shot.get_position(), tap_pos, tap_shot.get_global_position()]))
	# tap_shot.add_collision_exception_with(self)
	_attacking_mutex.unlock()


func trace_drag(_event:InputEventSingleScreenDrag) -> void:
	var ev_pos = _touch_to_camera_coords(_event.position)
	if not _initial_drag_set:
		if _attacking_mutex.try_lock() != OK:
			return
		_init_drag_pos = ev_pos
		_initial_drag_set = true
		_drag_shot_trace = null
		return
	else:
		if _drag_shot_trace == null:
			_drag_shot_trace = _drag_shot_trace_scn.instance()
			_drag_shot_trace.init(_init_drag_pos)
			get_parent().add_child(_drag_shot_trace)
		# print("Updated trace scale")
		_drag_shot_trace.update_scale(ev_pos)
		return


# Create an instance of Drag_Shot attack, recording the first touched position,
 # until the point where the player lets go.
 #	Returns: void
func _on_ReleaseDragTimer_timeout()->void:
	if _drag_shot_trace != null:
		# print("Drag from:\t{0}\nto:\t{1}".format([
		# 	_drag_shot_trace.position,
		# 	_drag_shot_trace.last_direction
		# 	])
		# )
		_initial_drag_set = false
		_attacking_mutex.unlock()
		_drag_shot_trace.queue_free()


# Create an instance of Drag_Shot attack, recording the first touched position,
 # until the point where the player lets go.
 #	Returns: void
func touch_attack(_event:InputEventSingleScreenTouch)->void:
	# print("On Touch Attack:")
	if _initial_drag_set:
		if not _event.is_pressed():
			_release_drag_timer.start(_trace_wait_time)


# Create an instance of Drag_Shot attack, recording the first touched position,
 # until the point where the player lets go.
 #	Returns: void
func zoom_attack(_event)->void:
	if _attacking_mutex.try_lock() != OK:
		return
	# print(_event.as_text())
	_attacking_mutex.unlock()
