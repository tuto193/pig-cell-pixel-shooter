class_name Spawner
extends Position2D



var Rusher:PackedScene = load("res://chars/Rusher.tscn")
var Ranger
var Heavy
var _rng = RandomNumberGenerator.new()
# TODO (Once more enemies added):
# -> (pre)load their packed scenes here and add their types and probabilities to the dict.

# Make sure the probabilities are always sorted from highest to lowest
var EnemyProbs:Dictionary = {
	"rusher" : [1.0, Rusher],
	"ranger" : [0.0, Ranger],
	"heavy" : [0.0, Heavy],
}

func _ready() -> void:
	# check the amount of enemy types and their respective probabilities
	# add up:
	#   n enemies, with m probs, n = m
	#   Sum over all probs in m should equal to 1.0
	var total = 0
	for p in EnemyProbs.values():
		total += p[0]
	assert(total == 1.0)
	_rng.randomize()

func spawn_single_enemy(enemy_type:String = "rusher") -> void:
	var offset = Vector2(
		_rng.randf_range(-1.0, 1.0),
		_rng.randf_range(-1.0, 1.0)
	)
	var enemy = EnemyProbs[enemy_type][1].instance()
	enemy.position = (self.position + offset)
	self.get_parent().add_child(enemy)


func spawn_probably() -> void:
	var previous_probability:float = 0
	var random_number:float = _rng.randf()
	for enemy_name in EnemyProbs.keys():
		previous_probability += EnemyProbs[enemy_name]
		if random_number < previous_probability:
			spawn_single_enemy(enemy_name)
			return


func spawn_n_enemies(n:int=1) -> void:
	for _i in range(n):
		spawn_probably()
