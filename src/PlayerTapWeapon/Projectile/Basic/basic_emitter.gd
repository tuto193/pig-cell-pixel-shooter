class_name BasicEmitter
extends ProjectileEmitter


var projectile := preload("BasicProjectile.tscn")

onready var timer := $Timer
onready var ANGLE_TO_MIDDLE:float = self.rotation



func _ready() -> void:
	var _error = InputManager.connect("single_tap", self, "_on_single_tap")


# Attempts to call the fire function on tap
func _on_single_tap(_event:InputEventSingleScreenTap) -> void:
	# adjust the angle regardless of shot fired
	var _new_reference_angle:Vector2 = _event.position - self.global_position
	var old_rot = self.rotation
	self.rotation = Vector2.UP.angle_to(_new_reference_angle) - ANGLE_TO_MIDDLE
	print(
		"Tapped at:\t{0}\n".format([_event.position]),
		"Emit. pos:\t{0}\n".format([self.position]),
		"Emit gpos:\t{0}\n".format([self.global_position]),
		"Emit rrot:\t{0}\n".format([ANGLE_TO_MIDDLE]),
		"Emit orot:\t{0}\n".format([old_rot]),
		"Emit nrot:\t{0}\n".format([self.rotation])
	)
	if timer.is_stopped():
		fire()
		timer.start(1.0 / projectiles_per_second)


func _physics_process(_delta: float) -> void:
	if Input.is_action_pressed("fire") and timer.is_stopped():
		fire()
		timer.start(1.0 / projectiles_per_second)


# Spawns and configures a basic projectile scene and connects to its signals.
func _do_fire(_direction: Vector2, _motions: Array, _lifetime: float) -> void:
	if not spawned_objects:
		return

	var new_projectile := projectile.instance()
	new_projectile.setup(global_position, _direction, _motions, _lifetime)
	spawned_objects.add_child(new_projectile)

	var _error := new_projectile.connect("collided", self, "_on_projectile_collided")
	_error = new_projectile.connect("missed", self, "_on_projectile_missed")
